#/bin/sh
#
#PBS -l nodes=1:ppn=1
#PBS -q serial
#
module load python/3.2.3-gcc
cd $PBS_O_WORKDIR
echo $HOSTNAME
echo "Hosts:"
cat $PBS_NODEFILE
echo ""
#
time mpiexec -n 1 python3 search_twitter_data.py ../../data/Twitter.csv 
