import sys
import csv
import _csv
import json
import re
import operator
import subprocess
from itertools import islice

from mpi4py import MPI
import lib.ttp as ttp

def count_term(tweets, term):
    count = 0
    for tweet in tweets:
        count += sum([ 1 for word in tweet.words if word.lower() == term.lower() ])
    return count

def get_users_for_tweet(tweet):
    return tweet.users

def get_tags_for_tweet(tweet):
    return tweet.tags

def count_x(tweets, get_x_for_tweet):
    x_counts = {}
    for tweet in tweets:
        for x in get_x_for_tweet(tweet):
            try:
                x_counts[x.lower()] += 1
            except KeyError:
                x_counts[x.lower()] = 1
    return x_counts

def merge_counts(counts1, counts2):
    merged_counts = {}
    for (key, count) in counts1.items():
        try:
            merged_counts[key] += count
        except KeyError:
            merged_counts[key] = count
    for (key, count) in counts2.items():
        try:
            merged_counts[key] += count
        except KeyError:
            merged_counts[key] = count
    return merged_counts

def parse_twitter_data(filename, start_row, end_row):
    with open(filename, 'rt') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        value_col = next(csv_reader).index('value')
        tweets = []
        tweet_parser = ttp.Parser()
        csv_iter = islice(csv_reader, start_row, end_row + 1)
        while True:
            try:
                row = next(csv_iter)
                tweet_raw = json.loads(row[value_col])['text']
                tweet = tweet_parser.parse(tweet_raw)
                tweet.words = re.sub(r'[^\w\s]', '', tweet_raw).split(' ')
                tweets.append(tweet)
            except _csv.Error:
                pass
            except StopIteration:
                break

    return tweets 

def file_len(filename):
    p = subprocess.Popen(['wc', '-l', filename], stdout=subprocess.PIPE, 
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def split(a, n):
    k, m = int(len(a) / n), int(len(a) % n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

def receive_merge_counts(comm, size):
    merged_counts = {}
    for proc in range(1, size):
        merged_counts = merge_counts(merged_counts, comm.recv(source=proc))
    return merged_counts

def print_top_n(counts, n):
    print (sorted(counts.items(),
           key=operator.itemgetter(1),
           reverse=True) [0:n])

def main():
    if len(sys.argv) < 2:
        sys.stderr.write("USAGE: %s <twitter-data-file>\n" % sys.argv[0])
        sys.exit()

    filename = sys.argv[1]

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0 and size == 1:
        total_rows = file_len(filename)
        tweets = parse_twitter_data(filename, 0, total_rows - 1)
        print(count_term(tweets, "they"))
        print_top_n(count_x(tweets, get_users_for_tweet), 10)
        print_top_n(count_x(tweets, get_tags_for_tweet), 10)
    elif rank == 0 and size > 1:
        total_rows = file_len(filename)
        row_divisions = list(split(range(total_rows), size - 1))
        for proc in range(1, size):
            comm.send((row_divisions[proc-1][0], row_divisions[proc-1][-1]), dest=proc)

        term_count = 0
        for proc in range(1, size):
            term_count += comm.recv(source=proc)
        print(term_count)

        merged_mention_counts = receive_merge_counts(comm, size)
        print_top_n(merged_mention_counts, 10)
        merged_tag_counts = receive_merge_counts(comm, size)
        print_top_n(merged_tag_counts, 10)
    else:
        (start_row, end_row) = comm.recv(source=0)
        tweets = parse_twitter_data(filename, start_row, end_row)

        comm.send(count_term(tweets, "they"), dest=0)
        comm.send(count_x(tweets, get_users_for_tweet), dest=0)
        comm.send(count_x(tweets, get_tags_for_tweet), dest=0)

if __name__ == "__main__":
    main()
